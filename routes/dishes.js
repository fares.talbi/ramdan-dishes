import express from "express";
import { readFile } from "fs/promises";
import { getUpcomingRamadan } from "../utils/prayersTime.js";

//Import dishes data
const dishes = JSON.parse(
  await readFile(new URL("../data/dishes.json", import.meta.url))
);

const router = express.Router();

//Cooking Time
router.get("/cooktime", async (req, res, next) => {
  try {
    let { ingredient, day } = req.query;

    //Check Query Params
    if (!ingredient || !day) {
      return res
        .status(400)
        .json({ message: "Please enter ingredient and day" });
    }

    if (day.length !== 2 || day < 1 || day > 30) {
      return res
        .status(400)
        .json({ message: "Please enter a valid day in this format 'DD' " });
    }

    //Get dishesh how contains ingredient query
    let newDishes = dishes.filter((el) =>
      el.ingredients.find((el) => el.toUpperCase() == ingredient.toUpperCase())
    );

    if (newDishes.length === 0) {
      return res.status(400).json({
        message:
          "No dishes with this ingredient, please verify the ingredient query",
      });
    }

    //Get current month, year & time
    let currentDate = new Date();
    let month = currentDate.getMonth() + 1;
    let year = currentDate.getFullYear();

    //Get Upcoming Ramadan
    const upcomingRamadanCalender = await getUpcomingRamadan(month, year);

    //Get asr and maghrib prayer time of day of ramadan
    let dayRamadan = upcomingRamadanCalender.data.filter(
      (el) => el.date.hijri.day == day
    );
    let asr = dayRamadan[0].timings.Asr.split(" ")[0];
    let maghrib = dayRamadan[0].timings.Maghrib.split(" ")[0];

    //get Differnce between asr and maghrib
    const date = new Date();
    let dateAsr = date.setHours(asr.substr(0, 2), asr.substr(3, 2));
    let dateMaghrib = date.setHours(maghrib.substr(0, 2), maghrib.substr(3, 2));
    let diff = (dateMaghrib - dateAsr) / 60 / 1000;

    //Return the final dishes
    let finalDishes = [];
    let cooktime = 0;
    let message;
    newDishes.map((el) => {
      if (el.duration < diff - 15) {
        cooktime = diff - el.duration - 15;
        message = `${cooktime} minutes after Asr`;
      } else {
        cooktime = el.duration - diff + 15;
        message = `${cooktime} minutes before Asr`;
      }

      finalDishes.push({
        name: el.name,
        ingredients: el.ingredients,
        cooktime: message,
      });
    });

    return res.status(200).json({ finalDishes, asr, maghrib });
  } catch (error) {
    return res.status(400).json(error);
  }
});

router.get("/suggest", async (req, res, next) => {
  try {
    const day = req.query.day;
    let dish;
    let random = Math.floor(Math.random() * (22 - 0 + 1));

    //Check Query
    if (!day || day.length !== 2 || day < 1 || day > 30) {
      return res
        .status(400)
        .json({ message: "Please enter a valid day in this format 'DD' " });
    }
    console.log(random);
    dish = dishes[random];

    return res.status(200).json({ dish });
  } catch (error) {
    return res.status(400).json(error);
  }
});

export default router;
