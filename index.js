import express from "express";
import dishes from "./routes/dishes.js";
const app = express();
app.use(express.json());

//Routes
app.use("/", dishes);

//Server
app.listen(3000, () => {
  console.log(`App running on port 3000`);
});
