import fetch from "node-fetch";

export async function getPrayers() {
  const prayers = await fetch(
    "http://api.aladhan.com/v1/calendar?latitude=51.508515&longitude=-0.1254872&method=1&month=4&year=2017"
  );
  let response = await prayers.json();
  return response;
}

export async function getUpcomingRamadan(month, year) {
  let ramadanDay = undefined;
  let newMonth = month;
  let newYear = year;
  let hijriMonth = 0;
  let hijriYear = 0;

  //Get The 1st day date of upcoming ramadan
  while (!ramadanDay) {
    const prayers = await fetch(
      `http://api.aladhan.com/v1/calendar?latitude=33.892166&longitude=9.561555499999997&method=1&month=${newMonth}&year=${newYear}`
    );
    let response = await prayers.json();
    const ramadan = response.data.find(
      (el) => el.date.hijri.month.en == "Ramaḍān" && el.date.hijri.day === "01"
    );
    ramadanDay = ramadan;
    if (newMonth == 12) {
      newMonth = 1;
      newYear++;
    } else {
      newMonth++;
    }
    // console.log(newMonth, newYear);
  }

  if (ramadanDay) {
    hijriYear = ramadanDay.date.hijri.year;
    hijriMonth = ramadanDay.date.hijri.month.number;
  }

  //Get Upcoming Ramadan Calander
  const upcomingRamadanCalender = await fetch(
    `http://api.aladhan.com/v1/hijriCalendar?latitude=33.892166&longitude=9.561555499999997&method=1&month=${hijriMonth}&year=${hijriYear}`
  );
  let response = await upcomingRamadanCalender.json();
  //   console.log(hijriYear);

  return response;
}
